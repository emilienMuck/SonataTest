<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testCategory(): void
    {
        $cat = new Category();
        $cat->setName("test");



        $this->assertTrue($cat->getName()==='test');

    }
}
