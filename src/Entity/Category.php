<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository", repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @ORM\OneToMany(targetEntity="BlogPost", mappedBy="category")
     */
    private $blogPosts;

    /**
     * @ORM\OneToMany(targetEntity=BlogPost::class, mappedBy="id_category")
     */
    private $id_blog_post;

    public function __construct()
    {
        $this->blogPosts = new ArrayCollection();
        $this->id_blog_post = new ArrayCollection();
    }

    public function getBlogPosts()
    {
        return $this->blogPosts;
    }

    /**
     * @return Collection|BlogPost[]
     */
    public function getIdBlogPost(): Collection
    {
        return $this->id_blog_post;
    }

    public function addIdBlogPost(BlogPost $idBlogPost): self
    {
        if (!$this->id_blog_post->contains($idBlogPost)) {
            $this->id_blog_post[] = $idBlogPost;
            $idBlogPost->setIdCategory($this);
        }

        return $this;
    }

    public function removeIdBlogPost(BlogPost $idBlogPost): self
    {
        if ($this->id_blog_post->removeElement($idBlogPost)) {
            // set the owning side to null (unless already changed)
            if ($idBlogPost->getIdCategory() === $this) {
                $idBlogPost->setIdCategory(null);
            }
        }

        return $this;
    }
}
